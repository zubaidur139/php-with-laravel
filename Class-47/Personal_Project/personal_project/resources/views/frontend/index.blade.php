<x-frontend.layouts.master>
<h1>Index Page</h1>


<h3><a href="{{route('db')}}">Dashboard</a></h3>


    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
    @foreach ($products as $product)
      <div class="col">
        <div class="card mb-4 rounded-3 shadow-sm">
          <div class="card-header py-3">
            <img src="" />
          </div>
          <div class="card-body">
            <h4 class="card-title pricing-card-title">{{ Str::limit($product->title, 20) }}</h4>
            <button type="button" class="w-100 btn btn-lg btn-primary">Add to Cart</button>
          </div>
        </div>
      </div>
    @endforeach 
    </div>

{{ $products->links() }}


</x-frontend.layouts.master>