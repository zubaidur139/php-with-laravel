<?php
    class Category{
        public $conn;
        public function __construct(){
            try{
                session_start();
                $this->conn= new PDO("mysql:host=localhost;dbname=project_crud","root","root");
            } catch (PDOException $exception){
                echo $exception->getMessage();
                $_SESSION['error']= $exception->getMessage();
                header('location:create.php');
            }
        }
    
        public function index(){
            $query = "select * from categories";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();

                $data = $stmt->fetchAll();
                return $data;
        }
        public function show($id){
            $query = "select * from categories where id = :category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'category_id' => $id
                ]);

                return $stmt->fetch();;
        }
        public function edit($id){
            $query = "select * from categories where id = :category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'category_id' => $id
                ]);

                return $stmt->fetch();;
        }
        public function store($data){
            try{
               
                $name = $data['name'];

                $query = "insert into categories(name) values
                (:category_name)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'category_name'=>$name,]);
                $_SESSION['message']= 'Sucessfully Created';
                header('location:../category/list.php');
            } catch (PDOException $exception){
                $_SESSION['error']= $exception->getMessage();
                header('location:../category/create.php');
            }
        }
        public function update($data){
            try{
                $id = $data['id'];
                $name = $data['name'];
                $query = "update categories set 
                name=:name where id=:category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'name'=>$name,
                    'category_id'=> $id
                ]);
                $_SESSION['message']= 'Sucessfully Updated';
                header('location:../category/list.php');
            } catch (PDOException $exception){
                $_SESSION['error']= $exception->getMessage();
                header('location:../category/edit.php?id=' . $id);
            }
        }
        public function destroy($id){
            try{
                $query = "delete from categories where id = :category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'category_id' => $id
                ]);
                $_SESSION['message'] = 'Sucessfully Deleted';
                header('location:index.php'); 
            }
            catch(PDOException $exception){
                $_SESSION['error']= $exception->getMessage();
                header('location:index.php');
            }
            

        }
    }