<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <style>
            .navigation{
                display: flex;
                justify-content: center;
                list-style: none;
        }
        .navigation a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
       .navigation a:hover{
            background-color: green;
        }
        </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product</title>
</head>
<body>
<div class="sticky-top">
        <nav class="navbar navbar-dark bg-dark">
                <!-- Navbar content -->
            <div class="col-md-12">
                <div class="navigation" >
                    <a class="navbar-brand nav-link" href="">HOME</a>
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-bs-toggle="dropdown" >
                                Dropdown link
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <a class="navbar-brand" href="">All PRODUCTS</a>
                </div>
            </div>
        </nav>
    </div>
    <div class="container">
        <br>
        <div class="mb-3">
            <a class="btn btn-primary mb-3" href="list.php">Back</a>
        </div>
        <div style="display: grid;
                justify-content: center;">
                    <h1 style="padding: 0; margin: 0;">EDIT PRODUCT</h1>
        </div>
    <?php
    include_once '../../src/Product/Product.php';

    $productObject = new Product();
    $product = $productObject->edit($_GET['id']);
    ?>
    <?php
        if(isset( $_SESSION['error'])){
            echo $_SESSION['error'];
            unset($_SESSION['error']);
        }
       
    ?>
    <form action="update.php" method="post">
    <div class="mb-3">
        <input type="hidden" name="id" value="<?=$product['id']?>"><br>
    </div>
    <div class="mb-3">
        <label for="name"  class="form-label">Product Name:</label><br>
        <input type="text" class="form-control" name="name" value="<?=$product['name']?>" placeholder="Enter Product name"><br>
    </div>    
    <div class="mb-3">
        <label for="name"  class="form-label">Stock:</label><br>
        <input type="number" class="form-control" name="stock" value="<?=$product['stock']?>" placeholder="Enter Product Qty"><br>
    </div>    
    <div class="mb-3">
        <label for="name"  class="form-label">Price:</label><br>
        <input type="number" class="form-control" name="price" value="<?=$product['price']?>" placeholder="Enter Product price"><br>
    </div>
    <div class="mb-3">   
        <label for="">Description</label><br>
        <textarea name="description" class="form-control" id="description" value="<?= $product['description']?>" cols="30" rows="10" placeholder="Enter description"></textarea>
    </div>  
    <div class="mb-3">  
        <button class="btn btn-primary mb-3" type="submit">Update</button>
    </div>    
    </form>
</body>
</html>