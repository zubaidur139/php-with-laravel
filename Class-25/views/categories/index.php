

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category List</title>
</head>
<body>



<?php 

include_once '../../src/category.php';
$categoryobject = new Category();
$categories = $categoryobject-> index();
// echo '<pre>';
// print_r($categories);
// echo '</pre>';
  ?>


<a href="create.php">Add New</a>  
 <?php
    session_start();
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION ['message']);
    }
 ?>

    <table border ="1">
        <thead>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Description</th>
            </tr>
        </thead>

        <tbody>

            <?php
            $sl = 1;
            foreach($categories as $category){ ?>
            
            <tr>
                <td><?= $sl++ ?></td>
                <td><?= $category ['title'] ?></td>
                <td><?php echo $category ['description'] ?></td>
            </tr>
            <?php }?>


        >
        </tbody>
    </table>


</body>
</html>