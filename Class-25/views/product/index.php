
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
</head>
<body>



<?php 

include_once '../../src/product.php';
$productObject = new product();
$product = $productObject-> index();
// echo '<pre>';
// print_r($product);
// echo '</pre>';
  ?>


<a href="create.php">Add New</a>  
 <?php
    session_start();
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION ['message']);
    }
 ?>

    <table border ="1">
        <thead>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Category</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>

            <?php
            $sl = 1;
            foreach($product as $product){ ?>
            
            <tr>
                <td><?= $sl++ ?></td>
                <td><?= $product ['title'] ?></td>
                <td><?= $product ['category'] ?></td>
                <td><?php echo $product ['description'] ?></td>
                <td>
                    <a href="show.php?id=<?= $product ['category'] ?>">Show</a>
                 |
                 <a href="">Edit</a> 
                  |
                  <a href="">Delete</a> </td>
            </tr>
            <?php }?>


        >
        </tbody>
    </table>


</body>
</html>