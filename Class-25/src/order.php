<?Php
class Order
{
    public $conn;
    public function __construct()

    {
        try
        {
            $this->conn = new PDO("mysql:host=localhost; dbname=seip_b4","root", "root");
        }
        catch (PDOException $exception) 
        {
            session_start();
            $_SESSION['error']="Failed to connect, ERROR: ".$exception->getMessage();
            header("location:../views/orders/index.php");
        }
    }

    public function index()
    {
        $query = "select * from orders";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
    

        return $data;
    }

    public function store ($data)
    {
        print_r($data);
        $name = $data['name'];
        $product_name = $data['product_name'];
        $description = $data['description'];


        try
        {
            $query = "insert into orders(name, product_name, description) values(:orders_name, :orders_product_name, :orders_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'orders_name'=> $name,
                'orders_product_name'=>$product_name,
                'orders_description' => $description
    
            ]);


            $_SESSION['message']="Successfully created ";
            header("location: ../orders/index.php");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error']="Data submission faild, ERROR: ".$exception->getMessage();
            header("location: ../orders/create.php");
         }
    
    }
   
    

}





?>