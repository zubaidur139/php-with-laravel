<?Php
class Employee
{
    public $conn;
    public function __construct()

    {
        try
        {
            $this->conn = new PDO("mysql:host=localhost; dbname=seip_b4","root", "root");
        }
        catch (PDOException $exception) 
        {
            session_start();
            $_SESSION['error']="Failed to connect, ERROR: ".$exception->getMessage();
            header("location:../views/Employee/index.php");
        }
    }

    public function index()
    {
        $query = "select * from employee";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
    

        return $data;
    }

    public function store ($data)
    {
        print_r($data);
        $Last  = $data['Last Name'];
        $first = $data['First Name'];
        $Gender = $data['Gender'];
        $title = $data['title'];
        $age = $data['age'];
        


        try
        {
            $query = "insert into employee(last name, first name, Gender, title, age) values(:employee_last name, :employee_first name, :employee_Gender, :employee_title, :employee_age)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'employee_Last Name'=> $Last,
                'employee_first name'=> $first,
                'employee_Gender'=> $Gender,
                'employee_title'=> $title,
                'employee_age'=> $age,
                
    
            ]);


            $_SESSION['message']="Successfully created ";
            header("location: ../employee/index.php");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error']="Data submission faild, ERROR: ".$exception->getMessage();
            header("location: ../employee/create.php");
         }
    
    }
   
    

}





?>