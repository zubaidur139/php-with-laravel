<?Php
class Category
{
    public $conn;
    public function __construct()

    {
        try
        {
            $this->conn = new PDO("mysql:host=localhost; dbname=seip_b4","root", "root");
        }
        catch (PDOException $exception) 
        {
            session_start();
            $_SESSION['error']="Failed to connect, ERROR: ".$exception->getMessage();
            header("location:../views/categories/index.php");
        }
    }

    public function index()
    {
        $query = "select * from category";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
    

        return $data;
    }

    public function store ($data)
    {
        print_r($data);
        $title = $data['title'];
        $description = $data['description'];


        try
        {
            $query = "insert into category(title, description) values(:category_title, :category_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title'=> $title,
                'category_description' => $description
    
            ]);


            $_SESSION['message']="Successfully created ";
            header("location: ../categories/index.php");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error']="Data submission faild, ERROR: ".$exception->getMessage();
            header("location: ../categories/create.php");
         }
    
    }
   
    

}





?>