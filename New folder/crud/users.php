<?php
    include('db.php');
    include ('assets/includes/header.php') ;

    $sql = "SELECT * FROM login_php.user";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<h3>USERS LIST</h3>
<table border="1">
    <thead>
        <tr>
            <th>Ser No.</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1 ?>
        <?php foreach ($users as $user) {?>
        <tr>
            <td><?= $i++ ?></td>
            <td><?php echo $user['fname']?></td>
            <td><?php echo $user['email']?></td>
            <td>
                <button><a href="show_users.php?user_id=<?= $user['uid']?>">SHOW</a></button>
                <button><a href="edit_users.php?user_id=<?= $user['uid']?>">EDIT</a></button>
                <button>DELETE</button>
            </td>
        </tr>
        <?php } ;?>
    </tbody>
</table>