<x-backend.layouts.master>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
           All colors
        </div>
        <div><a href="{{route('dashboard.colors.create')}}"><button class="btn btn-outline-success">Add new color</button></a></div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Title</th>
                        
                        <th>Action</th>
                       
                  
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($colors as $color)
                        
                   
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{Str::limit($color->title, 20) }}</td>
                        
                        <td>
                            <a href=""><button class="btn btn-primary">view</button></a>
                            <a href=""><button class="btn btn-warning">edit</button></a>
                            <a href=""><button class="btn btn-danger">delete</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>