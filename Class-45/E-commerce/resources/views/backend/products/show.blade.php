<x-backend.layouts.master>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
           Products details
        </div>
        <div><a href="{{route('dashboard.products')}}"><button class="btn btn-outline-success">Product list</button></a></div>
        <div class="card-body">
           <h3> Category:{{$product->category->title}}</h3>
           <h3> Title:{{$product->title}}</h3>
           <p> Description:{{$product->description}}</p>
           <p> Price:{{$product->price}}</p>
           <p> image:{{$product->image}}</p>
           <img src="storage/app/public/.$product->image" />
        </div>
    </div>
</x-backend.layouts.master>