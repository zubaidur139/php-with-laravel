<x-backend.layouts.master>
    <div class="container">
        <h2>Create a product</h2>
        <a href="{{route('dashboard.products')}}"><button class="btn btn-outline-success">Product list</button></a>


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{route('dashboard.products.update', ['id'=>$product->id])}}" method="POST" class="py-3" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div>
                <label for="title" class="form-label">Category</label>
                <select name="category_id" id="" class="form-control">
                    <option value="" >Select one</option>

                    @foreach ($categories as $key=>$category)
                    <option value="{{$key}}" {{$product->category_id==$key ? 'selected' : ''}}>{{$category}}</option>
                    @endforeach
                   
                </select>
            </div>
            <div>
                <label for="exampleInputEmail1" class="form-label">Title</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="title" value="{{old('title',$product->title) }}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Description</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3"value="{ {old('description',$product->description) }}" ></textarea>
            </div>
            <div>
                <label for="exampleInputPassword1" class="form-label" >Price</label>
                <input type="number" name="price" class="form-control" id="exampleInputPassword1" value="{{old('price',$product->price) }}">
            </div>

            
            <div class="form-floating mt-3">
                <input type="file" name="image" class="form-control" id="inputImage">
                <label for="inputImage">Image</label>
                @error('image')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</x-backend.layouts.master>
