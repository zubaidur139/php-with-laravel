<x-frontend.layouts.master>

    @foreach ($categories as $key=> $category)
    <li><a class="dropdown-item" href="{{route('main', $key)}}">{{$categories}}</a></li>
    @endforeach

    <div class="container-fluid mt-3 mb-3">
        <div class="row g-2">

            @foreach ($products as $product)
                <div class="col-md-4">
                    <div class="card">
                        <div class="img-container">
                            {{-- <div class="d-flex justify-content-between align-items-center p-2 first"> <span
                                    class="percent">-25%</span> <span class="wishlist"><i
                                        class="fa fa-heart"></i></span> 
                                    </div> --}}
                            <img src="{{ asset('storage/products/' . $product->image) }}" alt="<b> there is no image</b>" class="img-fluid">
                        </div>
                        <div class="product-detail-container">
                            <div class="d-flex justify-content-between align-items-center">
                                <a href="">
                                <h6 class="mb-0">{{ Str::limit($product->title, 20) }}</h6></a> <span
                                    class="text-danger font-weight-bold">{{ $product->price }} Tk</span>
                            </div>
                            <div>
                                <p class="lead" style=" font-size: 15px;">{{ Str::limit($product->description, 100) }}</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mt-2">
                                <div class="ratings"> <i class="fa fa-star"></i> <span>4.5</span> </div>
                                <div class="size"> <label class="radio"> <input type="radio"
                                            name="size1" value="small"> <span>S</span> </label> <label
                                        class="radio"> <input type="radio" name="size1" value="Medium" checked>
                                        <span>M</span> </label> <label class="radio"> <input type="radio"
                                            name="size1" value="Large"> <span>L</span> </label> </div>
                            </div>
                            <div class="mt-3"> <button type="button" class="btn btn-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-cart" viewBox="0 0 16 16">
                                        <path
                                            d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z">
                                        </path>
                                    </svg>
                                    Cart
                                </button></div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
     </div>

<div style="display: flex;
justify-content: center;">
    {{$products->links()}}   

</div>

</x-frontend.layouts.master>
