<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    // use SoftDeletes;
    protected $fillable =['title', 'category_id','description','price','image'];

    public function category()
    {
        return $this->belongsto(category::class,'category_id');
    }
}
