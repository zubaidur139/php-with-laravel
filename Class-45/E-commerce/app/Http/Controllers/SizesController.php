<?php

namespace App\Http\Controllers;

use App\Models\Size;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SizesController extends Controller
{
    public function index()
    {
        $sizes =  Size::orderBy('id','desc')->get();
        return view('backend/sizes/index', compact('sizes'));
    }
    public function create()
    {
        return view('backend/sizes/create');
    }
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:sizes,title',
                    'description' => 'required|min:20|max:200'   
                ]
            );
            Size::create($request->all());
            return redirect()->route('dashboard.sizes');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
}
