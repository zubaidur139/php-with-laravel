<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{ 
    public function index( $categoryId = null){

        $categories = Category::pluck('title', 'id')->toArray();
        if($categoryId){
            // $category = category::findOrFail($categoryId);
            // $products = $category->products;
            $products = Product::where('categoryid',$ctegoryId)->orderBy('id','desc')->paginate(6);

        }else{
            $products = Product::orderBy('id','desc')->paginate(6);
        }
       
        return view('welcome',compact('products', "categories"));
    }
}
