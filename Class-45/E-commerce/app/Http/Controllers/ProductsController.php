<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use PDF;


class ProductsController extends Controller
{
    public function index()
    {
        $products =  Product::orderBy('id','desc')->get();
        return view('backend/products/index', compact('products'));
    }
    public function create()
    {
    //    $imageName= request->file('image')->store('public/storage/products') ;

    //    dd($imageName);
        $categories = Category::pluck('title','id')->toArray();
        // dd($categories);

        return view('backend/products/create',compect('categories'));

    }
    public function store(Request $request)
    {
        try {
            $requestData = $request->all();

            if ($request->hasFile('image')){


                $file = $request->file('image');
                $fileName = time(). '.' . $file->getClientOriginalExtension() ;

                Image::make($request->file('image'))
                ->resize(300, 200)
                ->save(storage_path().'/app/public/products/'.$fileName);
                $requestData ['image'] = $fileName;

            }

            
            // $request->validate(
            //     [
            //         'title' => 'required|max:30|unique:products,title',
            //         'description' => 'required|min:20|max:200',
            //         'price' => 'required|numeric',
            //         'image' => 'required'

            //     ]
            // );
            Product::create( $requestData);

            return redirect()->route('dashboard.products') ->withMessage('Successfully saved!');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }

    public function show(product $id)
    {
        
        // $product = product :: findorFail($id);
        // return view ('backend.products.show', compact('product'));

    }


    public function edit($id)
    {

        $categories = Category::pluck('title','id')->toArray();
        $product = product :: findorFail($id);
        return view ('backend.products.edit', compact('product', 'categories'));

    }

    public function update(Request $request,$id)
    {
        try{
            $requestData = $request->all();

            if ($request->hasFile('image')){


                $file = $request->file('image');
                $fileName = time().'.' . $file->getClientOriginalExtension() ;

                Image::make($request->file('image'))
                ->resize(300, 200);
                save(storage_path().'/app/public/products'.$fileName);
                $requestData ['image'] = $fileName;
            }
            else{
                $requestData ['image'] = $product->image;

            }


            $product = product :: findorFail($id);




            $product->update([
                'title' => $requestData ['title'],
                'price' => $requestData ['price'],
                'image' => $requestData ['image']
            ]);

                
            // $product->update($request->all());
            
            
            return redirect()->route('dashboard.products')->withMessage('Successfully!');

        }catch( QueryException $exception ){ 

       
        return redirect()->route('dashboard.products');

     }

    }
    public function destroy($id)
    {
        $product = product :: findorFail($id);
        $product->delete();
        return redirect()->route('dashboard.products') ->withMessage('Successfully deleted!');
    }

    public function trash()
    {
        $products = product::onlyTrashed()->get();
        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)
    {
        product::withTrashed()
        ->where ('id', $id)
        ->restore();
        return redirect()->route('products.trash')->withMessage('Restored Successfully');
    }
    
    public function delete()
    {
        product::withTrashed()
        ->where ('id', $id)
        ->forceDelete();
        return redirect()->route('products.trash')->withMessage('Deleted Successfully');
    }
     
    public function pdf()
    {
        $product = product::latest()->get();

        $pdf = PDF::loadView('backend.products.pdf', compect('products'));
        return $pdf->download('products.pdf');
       
    }
} 
