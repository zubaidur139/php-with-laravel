<?php
use App\Http\Controllers\task2;
use App\Http\Controllers\task3;
use App\Http\Controllers\task4;
use App\Http\Controllers\task1Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
// */

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/admin', function () {
    return view('backend.dashboard');
})->name('admin');

Route::get('/table', function () {
    return view('backend.table');
})->name('table');

Route::get('/index', function () {
    return view('frontend.index');
})->name('index');