
//https://phpenthusiast.com/object-oriented-php-tutorials/abstract-classes-and-methods/practice
<?php


class Animal
{
    function construct()

    {
        echo "This is Animal class";
    }
}
class Dog extends Animal
{
    function construct()
    {
        echo "This is dog class";
    }
}
$Dog = new Dog();
$Dog->construct();


?>