<?php include 'assets/includes/header.php' ?>

<div class="container">
<form action="assets/functions/data2.php" method="POST">
  <div class="container">
    <h1>Login!</h1>
    <hr>

    <label for="email"><b>Email</b></label>
    <input type="email" placeholder="Enter Email" name="email"  required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>

    <button type="submit" class="registerbtn">LOG IN</button>
  </div>
  
  <div class="container signin">
    <p>Dont have an account? <a href="signup.php">Sign Up</a>.</p>
  </div>
</form>
</div>

<?php include 'assets/includes/footer.php' ?>
