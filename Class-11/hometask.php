<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet">

    <title>Email template</title>

   

</head>
<body>
        <?php
        echo <<< HEADER
            
             <table  width="100%" >

        <tr>
            <td align="center">
                <table border="0" align="center" background-color="#C0C0C0">

                    <tr>
                        <td align="center"
                            style="color: #fff; font-size: 24px;  font-weight:700;letter-spacing: 3px; ">

                            <!-- section text ======-->

                            <div style="line-height: 100px; background-color:#DC143C; margin-bottom:15px;">

                                Welcome to the <span style="color: #5caad2;">TEAM GOOGLE</span>

                            </div>
                        </td>
                    </tr> 

                    <tr>
                        <td align="left">
                            <table border="0" width="590" align="center" cellpadding="0" cellspacing="0"
                                >
                                <tr>
                                    <td align="left"
                                        style="color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <!-- section text ======-->

                                        <p style="line-height: 24px; margin-bottom:15px;">

                                            Zubaidur,

                                        </p>
                                        <p style="line-height: 24px;margin-bottom:15px;">
                                            Great news, you will now be the first to see exclusive previews of our
                                            latest collections, hear about news from the Abacus!
                                            community and get the most up to date news in the world of fashion.
                                        </p>
                                        <p style="line-height: 24px; margin-bottom:30px;">
                                            You can access your account at any point using the link below.
                                        </p>
                                        <table border="0" align="center" width="180" cellpadding="0" cellspacing="0"
                                            bgcolor="#DC143C" style="margin-bottom:20px;">

                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center"
                                                    style="color: #fff; font-size: 14px;  line-height: 22px; letter-spacing: 1px;">


                                                    <div style="line-height: 22px;">
                                                        <a href="" style="color: #fff; text-decoration: none;">Activate My Account</a>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                        </table>
                                        <p style="line-height: 24px">
                                            Love,</br>
                                            The Google team
                                        </p>
                                        
                                        <p>Questions? Reply to this mail or contact us at <span style="color:#FF0000;"><u>zubaidur99@gmail.com</u></span></p> 

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table> 
            </div>             
        HEADER;   
        ?>        
</body>

</html>