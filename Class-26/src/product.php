<?Php
class Product
{
    public $conn;
    public function __construct()

    {
        try
        {
            $this->conn = new PDO("mysql:host=localhost; dbname=seip_b4","root", "root");
        }
        catch (PDOException $exception) 
        {
            session_start();
            $_SESSION['error']="Failed to connect, ERROR: ".$exception->getMessage();
            header("location:../views/product/index.php");
        }
    }

    public function index()
    {
        $query = "select * from products";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
    

        return $data;
    }

    public function store ($data)
    {
        print_r($data);
        $title = $data['title'];
        $category = $data['category'];
        $description = $data['description'];


        try
        {
            $query = "insert into products(title,category, description) values(:products_title, :products_category, :products_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'products_title'=> $title,
                'products_category'=>$category,
                'products_description' => $description
    
            ]);


            $_SESSION['message']="Successfully created ";
            header("location: ../product/index.php");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error']="Data submission faild, ERROR: ".$exception->getMessage();
            header("location: ../product/create.php");
         }
    
    }
   
    public function show($id)
    {
        $query = "select * from products where title = :products_title";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'products_title'=> $id,
                
    
            ]);
            return $stmt->fetch();
            
    
    }


    public function edit($id)
    {
        $query = "select * from products where title = :products_title";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'products_title'=> $id,
                
    
            ]);
            return $stmt->fetch();
            
    
    }

    public function update ($data)
    {
        print_r($data);
        $title = $data['title'];
        $category = $data['category'];
        $description = $data['description'];


        try
        {
            $query = "update products set title =:product_title, description = ;product_description";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'products_title'=> $title,
                'products_category'=>$category,
                'products_description' => $description
    
            ]);


            $_SESSION['message']="Successfully created ";
            header("location: ../product/index.php");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error']="Data submission faild, ERROR: ".$exception->getMessage();
            header("location: ../product/create.php");
         }
    
    }

}





?>