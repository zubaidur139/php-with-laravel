<x-backend.layouts.master>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
           All brands
        </div>
        <div><a href="{{route('dashboard.brands.create')}}"><button class="btn btn-outline-success">Add new brand</button></a></div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                       
                  
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($brands as $brand)
                        
                   
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{Str::limit($brand->title, 20) }}</td>
                        <td>{{Str::limit($brand->description, 50)}}</td>
                        <td>
                            <a href=""><button class="btn btn-primary">Show</button></a>
                            <a href=""><button class="btn btn-warning">Edit</button></a>
                            <a href=""><button class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>