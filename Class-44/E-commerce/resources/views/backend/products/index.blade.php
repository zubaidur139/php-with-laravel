<x-backend.layouts.master>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
           All products
        </div>
        <div><a href="{{route('dashboard.products.trash')}}"><button class="btn btn-sm btn-info">Trash List</button></a></div>
        <div><a href="{{route('dashboard.products.create')}}"><button class="btn btn-outline-success">Add new product</button></a></div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Action</th>
                       
                  
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($products as $product)
                        
                   
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{Str::limit($product->title, 20) }}</td>
                        <td>{{Str::limit($product->description, 50)}}</td>
                        <td>{{$product->price}}</td>
                        <td>
                            <a href="{{route('dashboard.products.show' ,['id'=>$product->id])}}"><button class="btn btn-primary">Show</button></a>
                            <a href="{{route('dashboard.products.edit' ,['id'=>$product->id])}}"><button class="btn btn-warning">Edit</button></a>
                            {{-- <a href=""></a> --}}

                            <form method="POST" 
                            action="{{route('dashboard.products.destroy' ,['id'=>$product->id])}}"style="display:inline" >
                            
                                @csrf
                                @method('delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>