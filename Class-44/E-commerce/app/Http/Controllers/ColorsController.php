<?php

namespace App\Http\Controllers;

use App\Models\Color;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ColorsController extends Controller
{
    public function index()
    {
        $colors =  Color::orderBy('id','desc')->get();
        return view('backend/colors/index', compact('colors'));
    }
    public function create()
    {
        return view('backend/colors/create');
    }
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:colors,title',
                    'description' => 'required|min:20|max:200'   
                ]
            );
            Color::create($request->all());
            return redirect()->route('dashboard.colors');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
}
