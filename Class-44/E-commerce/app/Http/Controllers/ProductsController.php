<?php

namespace App\Http\Controllers;
use App\Http\Controllers\ProductRequest;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use image;

class ProductsController extends Controller
{
    public function index()
    {
        $products =  Product::orderBy('id','desc')->get();
        return view('backend/products/index', compact('products'));
    }
    public function create()
    {
    //    $imageName= request->file('image')->store('public/storage/products') ;

    //    dd($imageName);
        return view('backend/products/create');
    }
    public function store(ProductRequest $request)
    {
        try {

            // if ($request->hasFile('image')){

            //     $image = Image::make($request->file('image'))->resize(300, 200);
            // }

            // dd($request->all())




            $request->validate(
                [
                    'title' => 'required|max:30|unique:products,title',
                    'description' => 'required|min:20|max:200',
                    'price' => 'required|numeric',
                    'image' => 'required'

                ]
            );
            Product::create($request->all());
            return redirect()->route('dashboard.products') ->withMessage('Successfully saved!');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }

    public function show($id)
    {

        $product = product :: findorFail($id);
        return view ('backend.products.show', compact('product'));
    }


    public function edit($id)
    {

        $product = product :: findorFail($id);
        return view ('backend.products.edit',compact('product'));

    }

    public function update(Request $request,$id)
    {
        try{
            $product = product :: findorFail($id);

            // $product->update([
            //     'title' => $request->title,
            //     'price' => $request->price,
            // ]);
            $product->update($request->all());

        }catch( QueryException $exception ){}


        return redirect()->route('dashboard.products');

    }
    public function destroy($id)
    {
        $product = product :: findorFail($id);
        $product->delete();
        return redirect()->route('dashboard.products') ->withMessage('Successfully deleted!');
    }

    public function trash()
    {
        $products = product::onlyTrashed()->get();
        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)
    {
        product::withTrashed()
        ->where ('id', $id,)
        ->restore();
        return redirect()->route('products.trash')->withMessage('Restored Successfully');
    }

    public function delete()


    {
        product::withTrashed()
        ->where ('id', $id,)
        ->forceDelete();
        return redirect()->route('products.trash')->withMessage('Deleted Successfully');
    }
}
