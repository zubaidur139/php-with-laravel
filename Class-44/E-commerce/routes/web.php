<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ColorsController;
use App\Http\Controllers\SizesController;
use App\Http\Controllers\BrandsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[FrontendController::class, 'index'])->name('main');





Route::get('/dashboard', function () {
    return view('backend/index');
})->name('dashboard');


Route::prefix('dashboard')->group(function (){

    Route::get('/deleted-products',[ProductsController::class, 'trash'])->name('dashboard.products.trash');
    Route::get('/deleted-products/{id}/restore',[ProductsController::class, 'restore'])->name('dashboard.products.restore');
    Route::delete('/deleted-products/{id}/',[ProductsController::class, 'delete'])->name('dashboard.products.delete');
    
    Route::get('/products',[ProductsController::class, 'index'])->name('dashboard.products');
    Route::get('/products/create',[ProductsController::class, 'create'])->name('dashboard.products.create');
    Route::post('/products/store',[ProductsController::class, 'store'])->name('dashboard.products.store');
    Route::get('/products/{id}',[ProductsController::class, 'show'])->name('dashboard.products.show');
    Route::get('/products/{id}/edit',[ProductsController::class, 'edit'])->name('dashboard.products.edit');
    Route::patch('/products/{id}',[ProductsController::class, 'update'])->name('dashboard.products.update');
    Route::delete('/products/{id}',[ProductsController::class, 'destroy'])->name('dashboard.products.destroy');
    

    Route::get('/categories',[ CategoriesController::class, 'index'])->name('dashboard.categories');
    Route::get('/categories/create',[ CategoriesController::class, 'create'])->name('dashboard.categories.create');
    Route::post('/categories/store',[ CategoriesController::class, 'store'])->name('dashboard.categories.store');

    Route::get('/colors',[ ColorsController::class, 'index'])->name('dashboard.colors');
    Route::get('/colors/create',[ ColorsController::class, 'create'])->name('dashboard.colors.create');
    Route::post('/colors/store',[ ColorsController::class, 'store'])->name('dashboard.colors.store');

    Route::get('/sizes',[ SizesController::class, 'index'])->name('dashboard.sizes');
    Route::get('/sizes/create',[ SizesController::class, 'create'])->name('dashboard.sizes.create');
    Route::post('/sizes/store',[ SizesController::class, 'store'])->name('dashboard.sizes.store');

    Route::get('/brands',[ BrandsController::class, 'index'])->name('dashboard.brands');
    Route::get('/brands/create',[ BrandsController::class, 'create'])->name('dashboard.brands.create');
    Route::post('/brands/store',[ BrandsController::class, 'store'])->name('dashboard.brands.store');

});




 

