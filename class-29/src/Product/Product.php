<?php
    class Product{
        public $conn;
        public function __construct(){
            try{
                session_start();
                $this->conn= new PDO("mysql:host=localhost;dbname=project_crud","root","root");
            } catch (PDOException $exception){
                echo $exception->getMessage();
                $_SESSION['error']= $exception->getMessage();
                header('location:create.php');
            }
        }
    
        public function index(){
            $query = "select * from products where is_deleted is null";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();

                $data = $stmt->fetchAll();
                return $data;
        }
        public function show($id){
            $query = "select * from products where id = :product_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'product_id' => $id
                ]);

                return $stmt->fetch();;
        }
        public function edit($id){
            $query = "select * from products where id = :product_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'product_id' => $id
                ]);

                return $stmt->fetch();;
        }
        public function store($data){
            try{
                $fileName = $_FILES['picture']['name'];
                $tempName = $_FILES['picture']['name'];
                $explodedArray = explode('.', $fileName);
                $fileExtension = end($explodedArray);
                $accpetableImageType = ['png','jpg','gif','jpeg'];

            if(!in_array($fileExtension, $accpetableImageType)){
                $_SESSION['error'] = 'Invalid image type. Only' .implode(',',$accpetableImageType).'Allowed';
                header('location:../views/create.php');
            }

            $uniqueImageName = time().$fileName;
            move_uploaded_file($tempName, '../../assets/images/' . $uniqueImageName);

                $name = $data['name'];
                $stock = $data['stock'];
                $price = $data['price'];
                $description =$data['description'];

                $query = "insert into products(name, stock, price, description, picture) values
                (:product_name,:product_stock,:product_price,:product_description,:product_picture)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'product_name'=>$name,
                    'product_stock'=>$stock,
                    'product_price'=>$price,
                    'product_description'=>$description,
                    'product_picture'=>$uniqueImageName,
                ]);
                $_SESSION['message']= 'Sucessfully Created';
                header('location:../product/list.php');
            } catch (PDOException $exception){
                $_SESSION['error']= $exception->getMessage();
                header('location:../product/create.php');
            }
        }
        public function update($data){
            try{
                $id = $data['id'];
                $name = $data['name'];
                $stock = $data['stock'];
                $price = $data['price'];
                $description =$data['description'];

                $query = "update products set 
                name=:name, 
                stock=:stock, 
                price=:price, 
                description=:description where id=:product_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'name'=>$name,
                    'stock'=>$stock,
                    'price'=>$price,
                    'description'=>$description,
                    'product_id'=> $id,
                ]);
                $_SESSION['message']= 'Sucessfully Updated';
                header('location:../product/list.php');
            } catch (PDOException $exception){
                $_SESSION['error']= $exception->getMessage();
                header('location:../product/edit.php?id=' . $id);
            }
        }
        public function destroy($id){
            try{
                $query = "delete from products where id = :product_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'product_id' => $id
                ]);
                $_SESSION['message'] = 'Sucessfully Deleted';
                header('location:../product/list.php'); 
            }
            catch(PDOException $exception){
                $_SESSION['error']= $exception->getMessage();
                header('location:../product/list.php');
            }
            

        }
    }