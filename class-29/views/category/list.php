<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <style>
            .navigation{
                display: flex;
                justify-content: center;
                list-style: none;
        }
        .navigation a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
       .navigation a:hover{
            background-color: green;
        }
        </style>
    <title>Category List</title>
</head>
<body>

<?php 
    include_once '../../src/Category/Category.php';

    $categoryObject = new Category();

    $categories = $categoryObject->index();
?>
    <div class="sticky-top">
        <nav class="navbar navbar-dark bg-dark">
                <!-- Navbar content -->
            <div class="col-md-12">
                <div class="navigation" >
                    <a class="navbar-brand nav-link" href="">HOME</a>
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-bs-toggle="dropdown" >
                                Dropdown link
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <a class="navbar-brand" href="">All PRODUCTS</a>
                </div>
            </div>
        </nav>
    </div>
<?php
        if(isset( $_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
       
?>
    <div class="container">
        <br>
    <div class="mb-3">
        <a class="btn btn-primary mb-3" href="create.php">Add New Category</a>
    </div>
    <table class="table table-success table-striped" style="width: 100%;">
        <thead >
            <tr>
                <th>SL</th>
                <th>Product Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sl = 1;
            foreach($categories as $category){?>
            <tr>
                <td><?php echo $sl++ ?></td>
                <td><?php echo $category['name']?></td>
                <td><a class="btn btn-primary" href="show.php?id=<?= $category['id']?>">Show</a>
                    <a class="btn btn-primary" href="edit.php?id=<?= $category['id']?>">Edit</a>
                    <a class="btn btn-danger" href="delete.php?id=<?= $category['id']?>">Delete</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
</body>
</html>