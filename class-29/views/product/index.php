<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List</title>
</head>
<body>
<?php 
    include_once 'Product.php';

    $productObject = new Product();

    $products = $productObject->index();
?>
<?php
        if(isset( $_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
       
    ?>
    <a href="create.php">Add New</a>
    <table border="1">
        <thead >
            <tr>
                <th>SL</th>
                <th>Product Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sl = 1;
            foreach($products as $product){?>
            <tr>
                <td><?php echo $sl++ ?></td>
                <td><?php echo $product['name']?></td>
                <td><a href="show.php?id=<?= $product['id']?>">Show</a>
                    <a href="edit.php?id=<?= $product['id']?>">Edit</a>
                    <a href="delete.php?id=<?= $product['id']?>">Delete</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>