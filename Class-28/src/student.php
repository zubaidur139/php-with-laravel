<?Php
class Student
{
    public $conn;
    public function __construct()

    {
        try
        {
            $this->conn = new PDO("mysql:host=localhost; dbname=seip_b4","root", "root");
        }
        catch (PDOException $exception) 
        {
            session_start();
            $_SESSION['error']="Failed to connect, ERROR: ".$exception->getMessage();
            header("location:../views/student/index.php");
        }
    }

    public function index()
    {
        $query = "select * from students";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
    

        return $data;
    }

    public function store ($data)
    {
        print_r($data);
        // $id = $data['id'];
        $name = $data['name'];
        $department = $data['department'];
        $roll = $data['roll'];


        try
        {
            $query = "insert into students(name, department, roll) values( :students_name, :students_department, :students_roll)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                
                'students_name'=> $name,
                'students_department'=> $department,
                'students_roll'=> $roll
                
    
            ]);


            $_SESSION['message']="Successfully created ";
            header("location: ../student/index.php");
        }
        catch(PDOException $exception)
        {
            session_start();
            $_SESSION['error']="Data submission faild, ERROR: ".$exception->getMessage();
            header("location: ../student/create.php");
         }
    
    }
   
    

}





?>